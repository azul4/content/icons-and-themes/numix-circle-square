# numix-circle-square

Icon themes (Circle and Square) for Linux from the Numix project

https://github.com/numixproject/numix-icon-theme-circle

https://github.com/numixproject/numix-icon-theme-square

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/icons-and-themes/numix-circle-square.git
```

<br>

Packages generated here:


**numix-icon-theme-circle** and **numix-icon-theme-square**

